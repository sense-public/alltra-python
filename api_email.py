import requests
import mysql.connector
import datetime
from datetime import datetime
import time

while True:
    mydb = mysql.connector.connect(
    host="172.16.44.132",
    user="root",
    password="P@ssw0rdsit",
    database="DOL_PDPA"
    )

    now = datetime.now()
    mycursor = mydb.cursor()
    mycursor.execute("SELECT e.id_email,e.email_status,e.email_from,e.email_to,e.email_subject,e.email_content,e.email_files,e.email_location FROM TB_TR_PDPA_EMAIL as e where e.email_status = 3 limit 200")
    myresult = mycursor.fetchall()

    data_host = "https://pipr.dol.go.th"


    for x in myresult:

        data_id = x[0]

        if x[7] == "top":
            data_body = '<body style="margin:0px; background: #f8f8f8; "><div width="100%" style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;"><div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px"><div style="padding: 40px; background: #fff;"><table border="0" cellpadding="0" cellspacing="0" style="width: 100%;"><div style="border-bottom: 3px solid;color:rgb(240,185,11);font-weight: 600;font-size: xx-large;margin-bottom: 20px;"><p style="text-align: center;margin-bottom: 20px;">DOL PDPA</p></div><div style="text-align: center;margin-bottom: 10px;"><a href="{host}/agree-email/{id}" style="display: inline-block; padding: 5px 8px;  font-size: 15px; color: #fff; background: #39c449; border-radius: 5px; text-decoration:none;">ยินยอม </a> <a href="{host}/notagree-email_csv/{id}"style="display: inline-block; padding: 5px 8px;  font-size: 15px; color: #fff; background: #4fc3f7; border-radius: 5px; text-decoration:none;">ไม่ยินยอม </a></div><tbody><tr><td><p>{content}</p></td></tr></tbody></table></div></div></div></body>'.format(content=x[5],host=data_host,id=x[0])
        else:
            data_body = '<body style="margin:0px; background: #f8f8f8; "><div width="100%"style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;"><div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px"><div style="padding: 40px; background: #fff;"><table border="0" cellpadding="0" cellspacing="0" style="width: 100%;"><div style="border-bottom: 3px solid;color:rgb(240,185,11);font-weight: 600;font-size: xx-large;margin-bottom: 20px;"><p style="text-align: center;margin-bottom: 20px;">DOL PDPA</p></div><tbody><tr><td><p>{content}</p><div style="border-top: 3px solid;color: rgb(240,185,11);text-align: center;margin-top: 30px;"><a href="{host}/agree-email/{id}" style="display: inline-block; padding: 5px 8px; margin: 20px 0px 30px; font-size: 15px; color: #fff; background: #39c449; border-radius: 5px; text-decoration:none;">ยินยอม </a> <a href="{host}/notagree-email_csv/{id}" style="display: inline-block; padding: 5px 8px; margin: 20px 0px 30px; font-size: 15px; color: #fff; background: #4fc3f7; border-radius: 5px; text-decoration:none;">ไม่ยินยอม </a></div></td></tr></tbody></table></div></div></div></body>'.format(content=x[5],host=data_host,id=x[0])

        
        data_api = {
        "from":"pipr@dol.go.th",
        "to":x[3],
        "subject":x[4],
        "body":data_body,
        "attachment_url":["http://172.16.44.132:3000/files_upload_dev/files_upload/"+str(x[6])]
        }
        if x[6] == None:
            data_api.pop('attachment_url')
            print('None attachment_url')
        
        r = requests.post('http://172.16.42.126/SMTPMailPIPR/api/SMTPMailPIPR/SendMail', data = data_api)
        
        check_status = r.json()["status"]
        check_error = r.json()["message"]
        print(r.json()) #200
        print(now) #200

        if check_status == 200:
            print('true')
            mycursor = mydb.cursor()
            sql = "UPDATE TB_TR_PDPA_EMAIL as e SET e.email_status = 0,e.email_date_send = %s WHERE e.id_email = %s"
            val = (now, x[0])
            mycursor.execute(sql, val)
            mydb.commit()
        else:
            print('false')
            mycursor = mydb.cursor()
            sql = "UPDATE TB_TR_PDPA_EMAIL as e SET e.email_status = 4,e.email_date_send = %s,e.email_send_comment = %s WHERE e.id_email = %s"
            val = (now, check_error, x[0])
            mycursor.execute(sql, val)
            mydb.commit()
    mydb.close
    time.sleep(3600)
