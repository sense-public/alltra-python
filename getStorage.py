from datetime import datetime, timedelta
import mysql.connector
import subprocess as sub
import time

mydb = mysql.connector.connect(
    host="192.168.170.84",
    user="users",
    passwd="P@ssw0rd",
    database="DOL_PDPA_DEV"
)
time3 = (datetime.now() - timedelta(hours=1)).strftime("%Y-%m-%d %H:%M:%S")
while True:
    a = 'df -h -T --total'
    p = sub.Popen((a,), stdout=sub.PIPE, shell=True)
    for row in iter(p.stdout.readline, b''):
        mycursor = mydb.cursor()
        x = row.rstrip()
        u = str(x).find('total')
        if(u > -1):
            a = str(x).split()
            print(a)
            storage = a[2]
            storageused = a[3]
            storageper = a[5]
            print(storage,storageused)
            sql = "UPDATE `TB_TR_PERFORMANCE` SET `storage` = %s,`storageused`= %s,`storageper` = %s WHERE `TB_TR_PERFORMANCE`.`performance_id` = 1 ;"
            mycursor.execute(sql,(storage,storageused,storageper))
            myresult = mycursor.fetchall()
            mydb.commit()
    time.sleep(5)