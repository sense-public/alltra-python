from datetime import datetime, timedelta
import mysql.connector
import subprocess as sub
import time

mydb = mysql.connector.connect(
    host="192.168.170.84",
    user="users",
    passwd="P@ssw0rd",
    database="DOL_PDPA_DEV"
)
time3 = (datetime.now() - timedelta(hours=1)).strftime("%Y-%m-%d %H:%M:%S")
while True:
    a = 'free -m'
    p = sub.Popen((a,), stdout=sub.PIPE, shell=True)
    for row in iter(p.stdout.readline, b''):
        mycursor = mydb.cursor()
        x = row.rstrip()
        u = str(x).find('Mem')
        if(u > -1):
            a = str(x).split()
            mem = a[1]
            memused = a[2]
            mem = int(mem)*0.000977
            memused = int(memused)*0.000977
            memused = (memused*100)/mem
            print(mem,memused)
            sql = "UPDATE `TB_TR_PERFORMANCE` SET `mem` = %s,`memused`= %s WHERE `TB_TR_PERFORMANCE`.`performance_id` = 1 ;"
            mycursor.execute(sql,(mem,memused))
            myresult = mycursor.fetchall()
            mydb.commit()
        time.sleep(5)
