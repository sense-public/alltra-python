from datetime import datetime, timedelta
from tabnanny import check
import mysql.connector
import subprocess as sub
import time
import hashlib

mydb = mysql.connector.connect(
           host="192.168.170.84",
            user="users",
            passwd="P@ssw0rd",
            database="DOL_PDPA_DEV"
        )

while True:
    time1 = (datetime.now() - timedelta(hours=1)).strftime("%H:%M,%Y-%m-%d")
    time2 = (datetime.now() - timedelta(hours=1)).strftime("%Y-%m-%d")
    time4 = (datetime.now()).strftime("%H%M%S")
    #print(time1)
    # check device input with file log
    # a = 'ls /var/log/alltra|grep' + ' ".log"'
    a = 'ls /var/log/alltra|grep' +' "'+time1+'"'
    p = sub.Popen((a,), stdout=sub.PIPE, shell=True)
    for row in iter(p.stdout.readline, b''):
        print(time1)
        time3 = (datetime.now()).strftime("%Y-%m-%d %H:%M:%S")
        # x is file
        x = row.rstrip()
        z = str(x).split(',')
        # y is IP Adresse
        y = z[0].replace("b'", "")
        f = str(x).split('b')
        mvf = f[1]
        print(x)

        if x:
            with open(x, "rb") as f:
                bytes = f.read()
                hash = hashlib.sha256(bytes).hexdigest()
                print(hash)
                msg = hash.encode('ascii')

            mycursor = mydb.cursor()
            # check input
            sql1 = "SELECT input_ip  FROM TB_TR_INPUT WHERE input_ip = %s"
            val1 = (y)
            mycursor.execute(sql1, (val1,))
            input_ip = mycursor.fetchall()
            mydb.commit()
            print("input_ip:", input_ip)

            if len(input_ip) == 0:
                # insert input to device
                sql = "INSERT INTO `TB_TR_INPUT`  (`input_id`, `input_ip`, `date`,`hostname`,`type`) VALUES (NULL, %s, %s,%s,'udp')"
                mycursor.execute(sql, (y, time3, y))
                mydb.commit()

            sql2 = "SELECT device_id FROM `TB_TR_DEVICE` WHERE de_ip = %s"
            mycursor.execute(sql2, (y,))
            device_id = mycursor.fetchall()
            mydb.commit()
            print("device_id",(device_id))

            #check file readed
            if len(device_id) == 1:
                sql2 = "SELECT `file_id` FROM `TB_TR_FILE` WHERE `name` = %s AND `file_id` NOT IN (SELECT `file_id` FROM `TB_TR_READED` )"
                val1 = (x)
                mycursor.execute(sql2, (x,))
                checkfile = mycursor.fetchall()
                mydb.commit()
                print("checkfile_id",checkfile)

                #Insert log and file readed
                if len(checkfile) == 0:
                    sql = "INSERT INTO `TB_TR_FILE` (`file_id`, `date`, `name`,`hash`,`device_id`) VALUES (NULL, %s, %s,%s,%s)"
                    val1 = (time3)
                    val2 = (x)
                    val3 = (msg)
                    val66 = str(device_id[0][0])
                    mycursor.execute(sql, (time3, x, msg,val66))
                    mydb.commit()
                    print("insert file to DB ")

                    print("reading file")
                    file1 = open(x, 'r',encoding='utf-8',errors='ignore')
                    count = 0
                    while True:
                        count += 1
                        line = file1.readline()
                        if not line:
                            break
                        readline = str(
                            "{}".format(line.strip()))

                        sql = "INSERT INTO `TB_TR_LOG` (`log_id`, `msg`, `date`, `device_id`,`file_name`) VALUES (NULL, %s,%s,%s,%s)"
                        val1 = (readline)
                        val2 = time3
                        val3 = str(device_id[0][0])
                        mycursor.execute(sql, (val1, val2, val3, x))
                        mydb.commit()
                    file1.close()
                    print("read file successfully")
                    
                    sql1 = "SELECT `file_id` FROM `TB_TR_FILE` WHERE `name` = %s"
                    mycursor.execute(sql1, (x,))
                    file_id = mycursor.fetchall()
                    mydb.commit()
                    print("file_id",file_id)
                   
                    sql2 = "INSERT INTO `TB_TR_READED` (`readed_id`, `file_id`,`classify`) VALUES (NULL, %s,%s);"
                    val1 = (str(file_id[0][0]))
                    mycursor.execute(sql2, (val1,"0"))
                    mydb.commit()
                    print("readed")
                    b = 'mv '+mvf+' /var/log/alltra/readed/'
                    p = sub.Popen((b,), stdout=sub.PIPE, shell=True)  
                    print("file name: ",mvf)

        time.sleep(5)
