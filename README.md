## 1. วิธีการติดตั้ง Scripts บน Ubuntu

- 1.1 ติดตั้ง Python 3
- 1.2 ติดตั้ง PM2 และสั่ง Scripts ทำงาน

### 1.1 ติดตั้ง Python 3

```
apt-get update
apt-get install python3
python3 --version
pip3 install <libs name>
```
### 1.2 ติดตั้ง PM2 และสั่ง Scripts ทำงาน

```
apt-get update
apt-get install nodejs
apt-get install npm
npm install pm2@latest -g
pm2 --version
pm2 start <script name>.py --name <process name> --interpreter python3
pm2 save
pm2 startup
```
<br>

***
<br>

## 2. วิธีการติดตั้ง Scripts บน Oracle Linux

- 2.1 ติดตั้ง Python 3.9
- 2.2 ติดตั้ง PM2 และสั่ง Scripts ทำงาน

### 2.1 ติดตั้ง Python 3.9

```
python3 --version
yum remove python3
yum install python39
python3 -m venv .env
source .env/bin/activate
pip3 install <libs name>
```
### 2.2 ติดตั้ง PM2 และสั่ง Scripts ทำงาน

```
dnf install -y nodejs
npm install pm2@latest -g
pm2 --version
pm2 start <script name>.py --name <process name> --interpreter python3
pm2 save
pm2 startup
```
