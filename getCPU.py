from datetime import datetime, timedelta
import mysql.connector
import subprocess as sub
import time

mydb = mysql.connector.connect(
    host="192.168.170.84",
    user="users",
    passwd="P@ssw0rd",
    database="DOL_PDPA_DEV"
)
time3 = (datetime.now() - timedelta(hours=1)).strftime("%Y-%m-%d %H:%M:%S")
while True:
    a = 'sar 1 -h'
    p = sub.Popen((a,), stdout=sub.PIPE, shell=True)
    for row in iter(p.stdout.readline, b''):
        mycursor = mydb.cursor()
        x = row.rstrip()
        u = str(x).find('all')
        if(u > -1):
            a = str(x).split()
            i = len(a)-1
            cpu = a[i]
            cpuused = cpu.split('%')
            cpuused = cpuused[0]
            try:
                cpuused = float((100-float(cpuused)))
                cpuused = "%.2f"%round(cpuused, 2)
                #print(cpuused)
                #print(cpu)
                sql = "UPDATE `TB_TR_PERFORMANCE` SET `cpu` = %s,`cpuused`= %s WHERE `TB_TR_PERFORMANCE`.`performance_id` = 1 ;"
                mycursor.execute(sql,(cpu,cpuused))
                myresult = mycursor.fetchall()
                mydb.commit()
            except ValueError as e:
                print("Could not convert string to float: ", str(e))
        time.sleep(5)
